﻿using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using NawaDevDAL.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NawaDevBLL
{
    public class GetModuleDataService
    {
        private readonly ModuleRepository moduleRepository;
        private readonly SystemParameterRepository systemParameterRepository;

        public string Sql { get; private set; }

        public GetModuleDataService(
            ModuleRepository moduleRepository,
            SystemParameterRepository systemParameterRepository)
        {
            this.moduleRepository = moduleRepository;
            this.systemParameterRepository = systemParameterRepository;
        }

        public List<DynamicDictionary> GetModuleData(int page, string orderBy, string order, string search, string filter, out int rowCount, JSONTree jsonFilter = null)
        {
            rowCount = 0;

            try
            {
                string text = "Module";
                string text2 = "";
                if (!string.IsNullOrEmpty(search))
                {
                    text2 = " WHERE ( CAST(PK_Module_ID AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                    text2 = text2 + " OR CAST(ModuleName AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                    text2 = text2 + " OR CAST(ModuleLabel AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                    text2 = text2 + " OR CAST(ModuleDescription AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%'";
                    text2 = text2 + " OR CASE WHEN IsUseDesigner = 1 THEN 'true' WHEN IsUseDesigner = 0 THEN 'false' END LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%'";
                    text2 = text2 + " OR CASE WHEN Active = 1 THEN 'true' WHEN Active = 0 THEN 'false' END LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' )";
                }
                else if (!string.IsNullOrEmpty(filter))
                {
                    string[] array = filter.Split(' ');
                    string text3 = string.Join(" ", array.Skip(1));
                    string text4 = array[0];
                    text2 = ((string.IsNullOrEmpty(text4) || !(array[1] == "LIKE")) ? (" WHERE (" + filter.Replace('"', ' ') + ")") : ((!(text4 == "IsUseDesigner") && !(text4 == "Active")) ? (" WHERE ([" + array[0] + "] " + text3 + ")") : (" WHERE (CASE WHEN [" + array[0] + "] = 1 THEN 'true' WHEN [" + array[0] + "] = 0 THEN 'false' END " + text3 + ")")));
                }

                if (jsonFilter != null)
                {
                    text2 = "";
                    string text5 = AdvanceFilterBLL.JSONToQuery(jsonFilter);
                    if (!string.IsNullOrEmpty(text5))
                    {
                        text2 = " WHERE " + text5;
                    }
                }

                string value = "";
                string[] source = new string[30]
                {
                    "PK_Module_ID", "ModuleName", "ModuleLabel", "ModuleDescription", "IsUseDesigner", "IsUseApproval", "IsSupportAdd", "IsSupportEdit", "IsSupportDelete", "IsSupportActivation",
                    "IsSupportView", "IsSupportUpload", "IsSupportDetail", "UrlAdd", "UrlEdit", "UrlDelete", "UrlActivation", "UrlView", "UrlUpload", "UrlApproval",
                    "UrlApprovalDetail", "UrlDetail", "IsUseStoreProcedureValidation", "Active", "CreatedBy", "LastUpdateBy", "ApprovedBy", "CreatedDate", "LastUpdateDate", "ApprovedDate"
                };
                if (!string.IsNullOrEmpty(orderBy))
                {
                    string text6 = source.Where((string x) => x.ToLower() == orderBy.ToLower()).FirstOrDefault();
                    if (!string.IsNullOrEmpty(text6))
                    {
                        value = text6;
                        value = ((order == null || !(order.ToLower() == "desc")) ? (value + " asc") : (value + " desc"));
                    }
                }

                Sql = text + text2 + " order by " + value;

                // Get count row number
                rowCount = moduleRepository.GetCountRows(tableClause: text, filterClause: text2);

                int num = Convert.ToInt32(systemParameterRepository.GetAppSystemParameter(7).SettingValue);

                DataTable source2 = moduleRepository.GetPagingData(tableClause: text, filterClause: text2, orderBy: value, pageIndex: page, pageSize: num);

                return (from row in source2.AsEnumerable()
                        select new DynamicDictionary(row)).ToList();
            }
            catch (Exception ex)
            {
                return new List<DynamicDictionary>();
            }
        }
    }
}

﻿using Microsoft.Data.SqlClient;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NawaDevBLL
{
    public static class ModuleBLL
    {
        public static List<DynamicDictionary> GetModuleData(int page, string orderBy, string order, string search, string filter, out int rowCount, JSONTree jsonFilter = null)
        {
            string text = "Module";
            string text2 = "";
            if (!string.IsNullOrEmpty(search))
            {
                text2 = " WHERE ( CAST(PK_Module_ID AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                text2 = text2 + " OR CAST(ModuleName AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                text2 = text2 + " OR CAST(ModuleLabel AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                text2 = text2 + " OR CAST(ModuleDescription AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%'";
                text2 = text2 + " OR CASE WHEN IsUseDesigner = 1 THEN 'true' WHEN IsUseDesigner = 0 THEN 'false' END LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%'";
                text2 = text2 + " OR CASE WHEN Active = 1 THEN 'true' WHEN Active = 0 THEN 'false' END LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' )";
            }
            else if (!string.IsNullOrEmpty(filter))
            {
                string[] array = filter.Split(' ');
                string text3 = string.Join(" ", array.Skip(1));
                string text4 = array[0];
                text2 = ((string.IsNullOrEmpty(text4) || !(array[1] == "LIKE")) ? (" WHERE (" + filter.Replace('"', ' ') + ")") : ((!(text4 == "IsUseDesigner") && !(text4 == "Active")) ? (" WHERE ([" + array[0] + "] " + text3 + ")") : (" WHERE (CASE WHEN [" + array[0] + "] = 1 THEN 'true' WHEN [" + array[0] + "] = 0 THEN 'false' END " + text3 + ")")));
            }

            if (jsonFilter != null)
            {
                text2 = "";
                string text5 = AdvanceFilterBLL.JSONToQuery(jsonFilter);
                if (!string.IsNullOrEmpty(text5))
                {
                    text2 = " WHERE " + text5;
                }
            }

            string value = "";
            string[] source = new string[30]
            {
                    "PK_Module_ID", "ModuleName", "ModuleLabel", "ModuleDescription", "IsUseDesigner", "IsUseApproval", "IsSupportAdd", "IsSupportEdit", "IsSupportDelete", "IsSupportActivation",
                    "IsSupportView", "IsSupportUpload", "IsSupportDetail", "UrlAdd", "UrlEdit", "UrlDelete", "UrlActivation", "UrlView", "UrlUpload", "UrlApproval",
                    "UrlApprovalDetail", "UrlDetail", "IsUseStoreProcedureValidation", "Active", "CreatedBy", "LastUpdateBy", "ApprovedBy", "CreatedDate", "LastUpdateDate", "ApprovedDate"
            };
            if (!string.IsNullOrEmpty(orderBy))
            {
                string text6 = source.Where((string x) => x.ToLower() == orderBy.ToLower()).FirstOrDefault();
                if (!string.IsNullOrEmpty(text6))
                {
                    value = text6;
                    value = ((order == null || !(order.ToLower() == "desc")) ? (value + " asc") : (value + " desc"));
                }
            }

            string cmdText = "SELECT COUNT(13) AS [RowCount] FROM " + text + text2;
            rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(cmdText));
            // Get count row number

            int num = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);
            cmdText = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
            
            SqlParameter[] array2 = new SqlParameter[4]
            {
                    new SqlParameter("@Query", SqlDbType.VarChar)
                    {
                        Value = "SELECT * FROM " + text + text2
                    },
                    new SqlParameter("@OrderBy", SqlDbType.VarChar)
                    {
                        Value = value
                    },
                    new SqlParameter("@PageIndex", SqlDbType.Int)
                    {
                        Value = page
                    },
                    new SqlParameter("@PageSize", SqlDbType.Int)
                    {
                        Value = num
                    }
            };
            string cmdText2 = cmdText;
            IDataParameter[] cmdParams = array2;
            DataTable source2 = NawaDAO.ExecuteTable(cmdText2, cmdParams);
            return (from row in source2.AsEnumerable()
                    select new DynamicDictionary(row)).ToList();
        }
    }
}

﻿using NawaDevBLL;
using Xunit;

namespace WorkshopXunit
{
    public class CalculatorAddTests
    {
        [Fact]
        public void Test_InputPositiveNumber_ReturnPositiveNumber()
        {
            // Arrange
            var sut = new Calculator();

            // Act
            var result = sut.Add(1, 2);

            // Assert
            Assert.Equal(3, result);
        }

        [Fact]
        public void Test_InputNegativeNumber_ReturnNegativeNumber()
        {
            // Arrange
            var sut = new Calculator();

            // Act
            var result = sut.Add(-2, -2);

            // Assert
            Assert.Equal(-4, result);
        }
    }
}

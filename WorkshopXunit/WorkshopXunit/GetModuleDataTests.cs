﻿using Moq; //NSubtitute
using NawaDevBLL;
using NawaDevDAL.Repository;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using System.Data;

namespace WorkshopXunit
{
    public class GetModuleDataTests
    {
        private DataTable GetMockDataSet(bool withData = false)
        {
            var datatable = new DataTable();
            datatable.Columns.Add("PK_Module_ID");
            datatable.Columns.Add("ModuleName");

            if (withData)
            {
                var row = datatable.NewRow();
                row["PK_Module_ID"] = "10000";
                row["ModuleName"] = "Module";

                datatable.Rows.Add(row);
            }

            return datatable;
        }

        [Fact]
        public void Test_WithoutFilter_ShouldReturnData()
        {
            // Arrange
            var mockModuleRepo = new Mock<ModuleRepository>();
            mockModuleRepo.Setup(m => m.GetCountRows(It.IsAny<string>(), It.IsAny<string>())).Returns(1);
            mockModuleRepo.Setup(m => m.GetPagingData(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                          .Returns(GetMockDataSet(withData: true));

            var mockSysParamRepo = new Mock<SystemParameterRepository>();
            mockSysParamRepo.Setup(m => m.GetAppSystemParameter(It.IsAny<int>())).Returns(new AppSystemParameter() { SettingValue = "100" });

            var sut = new GetModuleDataService(mockModuleRepo.Object, mockSysParamRepo.Object);

            // Act
            var result = sut.GetModuleData(1, "CreatedDate", "DESC", "", "", out int rowCount);

            // Assert
            Assert.NotEmpty(result);
            Assert.Equal("Module", result[0].Dictionary["ModuleName"]);
            Assert.Equal("Module order by CreatedDate desc", sut.Sql);
        }

        [Fact]
        public void Test_WithoutFilterError_ShouldReturnEmpty()
        {
            // Arrange
            var mockModuleRepo = new Mock<ModuleRepository>();
            mockModuleRepo.Setup(m => m.GetCountRows(It.IsAny<string>(), It.IsAny<string>())).Returns(1);
            mockModuleRepo.Setup(m => m.GetPagingData(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                          .Throws(new Exception("DB Error"));

            var mockSysParamRepo = new Mock<SystemParameterRepository>();
            mockSysParamRepo.Setup(m => m.GetAppSystemParameter(It.IsAny<int>())).Returns(new AppSystemParameter() { SettingValue = "100" });

            var sut = new GetModuleDataService(mockModuleRepo.Object, mockSysParamRepo.Object);

            // Act
            var result = sut.GetModuleData(1, "CreatedDate", "DESC", "", "", out int rowCount);

            // Assert
            Assert.Empty(result);
        }
    }
}

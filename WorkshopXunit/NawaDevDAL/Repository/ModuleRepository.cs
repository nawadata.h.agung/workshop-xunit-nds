﻿using Microsoft.Data.SqlClient;
using NawaDataDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NawaDevDAL.Repository
{
    public class ModuleRepository
    {
        public virtual int GetCountRows(string tableClause, string filterClause)
        {
            string cmdText = "SELECT COUNT(13) AS [RowCount] FROM " + tableClause + filterClause;
            return Convert.ToInt32(NawaDAO.ExecuteScalar(cmdText));
        }

        public virtual DataTable GetPagingData(string tableClause, string filterClause, string orderBy, int pageIndex, int pageSize)
        {
            if (string.IsNullOrWhiteSpace(tableClause))
            {
                throw new ArgumentNullException(nameof(tableClause), "Table name is required");
            }

            string cmdText = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";

            SqlParameter[] array2 = new SqlParameter[4]
            {
                    new SqlParameter("@Query", SqlDbType.VarChar)
                    {
                        Value = "SELECT * FROM " + tableClause + filterClause
                    },
                    new SqlParameter("@OrderBy", SqlDbType.VarChar)
                    {
                        Value = orderBy
                    },
                    new SqlParameter("@PageIndex", SqlDbType.Int)
                    {
                        Value = pageIndex
                    },
                    new SqlParameter("@PageSize", SqlDbType.Int)
                    {
                        Value = pageSize
                    }
            };
            string cmdText2 = cmdText;
            IDataParameter[] cmdParams = array2;

            return NawaDAO.ExecuteTable(cmdText2, cmdParams);
        }
    }
}

﻿using NawaDataBLL;
using NawaDataDAL.Models;

namespace NawaDevDAL.Repository
{
    public class SystemParameterRepository
    {
        public virtual AppSystemParameter GetAppSystemParameter(int id)
        {
            return SystemParameterBLL.GetAppSystemParameter(id);
        }
    }
}

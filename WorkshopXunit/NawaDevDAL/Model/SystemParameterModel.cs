﻿namespace NawaDevDAL.Model
{
    public class SystemParameterModel
    {
        public long PK_SystemParameter_ID { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
    }
}
